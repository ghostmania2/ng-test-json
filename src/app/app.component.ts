import { Component } from '@angular/core';
import { ReadFileService } from './read-file.service';

export interface User {
  firstName: string;
  lastName: string;
  city: string;
  country: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) 

export class AppComponent{
  users:User[] = [];
  displayedColumns = ['userName', 'userLastname','city','country'];
  dataSource = [];
  loading: boolean = false;
  showErr: boolean = false;
  errorText: string = 'If you see this text, something went wrong with JSON file';
  
  constructor(
    private fileService: ReadFileService
  ){}
  

  ngOnInit(){
    this.readFile();
  }

  readFile() {
    if(!this.loading){
      this.loading = true;
      this.fileService.readJSON().subscribe((
        data: {Users:User[]}) => {
          this.users = data.Users; 
          this.dataSource = this.users;
          this.showErr ? this.showErr = false : '';
        },
        err => {
          this.showErr = true;    
          this.loading = false;
        },
        () => {
          this.loading = false;
        }
      )
    }
  }
}
