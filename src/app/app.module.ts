import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReadFileService } from './read-file.service';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {MatTableModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {MatButtonModule} from '@angular/material/button';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    CdkTableModule,
    MatButtonModule
  ],
  providers: [ReadFileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
