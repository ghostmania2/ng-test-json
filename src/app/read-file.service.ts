import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReadFileService {

  constructor(private http: HttpClient) { }

  readJSON(){
    return this.http.get('assets/fileToRead.json')
  }

}
